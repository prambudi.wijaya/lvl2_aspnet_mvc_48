﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportViewerForMvc;
using System.Web.UI.WebControls;

namespace LVL2_ASPNet_MVC_48.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReportEmployee()
        {
            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Pixel(1100);
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/ReportEmployee";
            Report.ServerReport.Refresh();
            ViewBag.ReportViewer = Report;

            return View();
        }

       

        public ActionResult ReportEmployeeSalary()
        {
            ReportViewer Report = new ReportViewer();
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.Width = Unit.Pixel(1100);
            string UrlReportServer = "http://localhost/ReportServer";
            Report.ProcessingMode = ProcessingMode.Remote;
            Report.ServerReport.ReportServerUrl = new Uri(UrlReportServer);
            Report.ServerReport.ReportPath = "/ReportEmployeeSalary";
            Report.ServerReport.Refresh();

            string id = "2";
            ReportParameter[] parameters = new ReportParameter[1];
            parameters[0] = new ReportParameter("id", id, true);
            Report.ServerReport.SetParameters(parameters);
            Report.ServerReport.Refresh();

            ViewBag.ReportViewer = Report;

            return View();
        }
    }

   
}